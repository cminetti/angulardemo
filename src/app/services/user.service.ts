import { Injectable } from '@angular/core';
import { Users } from '../interfaces/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  listUsers: Users[] = [
    { user: 'cminetti@globons.com', name: 'Camila', surname: 'Minetti', gender: 'Femenino' },
    { user: 'dbaranowski@globons.com', name: 'Diego', surname: 'Baranowski', gender: 'Masculino' },
    { user: 'maximiliano.sanchez@globons.com', name: 'Maximiliano', surname: 'Sanchez', gender: 'Masculino' },
    { user: 'cminetti@globons.com', name: 'Camila', surname: 'Minetti', gender: 'Femenino' },
    { user: 'dbaranowski@globons.com', name: 'Diego', surname: 'Baranowski', gender: 'Masculino' },
    { user: 'maximiliano.sanchez@globons.com', name: 'Maximiliano', surname: 'Sanchez', gender: 'Masculino' }
  ];
  
  constructor() { }

  getUsers() {
    return this.listUsers.slice()
  }

  deleteUser(index: number) {
    this.listUsers.splice(index, 1);
  }

  addUser(user: Users) {
    this.listUsers.unshift(user)
  }
}
