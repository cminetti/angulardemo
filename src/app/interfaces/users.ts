export interface Users {
    user: string,
    name: string,
    surname: string,
    gender: string
}