import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Users } from 'src/app/interfaces/users';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  genders: any[] = ['Masculino', 'Femenino'];
  form: FormGroup;

  constructor(private fb: FormBuilder, 
              private _userService: UserService,
              private router: Router,
              private _snackBar: MatSnackBar) {
    this.form = this.fb.group({
      user: ['', Validators.required],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      gender: ['', Validators.required]
    })
   }

  ngOnInit(): void {
  }

  addUser() {
    const user: Users = {
      user: this.form.value.user,
      name: this.form.value.name,
      surname: this.form.value.surname,
      gender: this.form.value.gender
    }
  this._userService.addUser(user)
  this.router.navigate(['/dashboard/users'])
  this._snackBar.open('Usuario agregado con éxito', '', {
    duration: 1500,
    horizontalPosition: 'center',
    verticalPosition: 'bottom'
  })
  }

}
